export const { format: formatPrice } = new Intl.NumberFormat('pt-BR', {
  styles: 'currency',
  currency: 'BRL'
});
